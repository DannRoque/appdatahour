package com.example.projeto1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
  Toolbar tollbar;
  Button btnData;
  Button btnHora;
  TextView txtData;
  TextView txtHora;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    setupToolbar();
    setupButtomHora();
    setupButtomData();
  }

  public void setupToolbar() {
    tollbar = findViewById(R.id.toolbar);
    setSupportActionBar(tollbar);
    setTitle("Meu Adaptador");
  }

  public void setupButtomData() {
    btnData = findViewById(R.id.buttonData);
    txtData = findViewById(R.id.labelData);
    btnData.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String data = sdf.format(new Date());
        txtData.setText(data);
      }
    });
  }

  private void setupButtomHora() {
    btnHora = findViewById(R.id.buttonHora);
    txtHora = findViewById(R.id.labelHora);
    btnHora.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String hour = sdf.format(new Date());
        txtHora.setText(hour);
      }
    });
  }
}
